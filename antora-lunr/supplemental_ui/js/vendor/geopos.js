document.addEventListener("DOMContentLoaded",function(){
  
  var latitud;
  var longitud;
    
  var estaciones=[];

  var getEstaciones = function(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function() {
      var status = xhr.status;
      if (status === 200) {
        callback(null, xhr.response);
      } else {
        callback(status, xhr.response);
      }
    };
    xhr.send();
  };

  function toRadians(degrees){
    var pi = Math.PI;
    return degrees * (pi/180);
  }

  function metersTo( lat1,  lng1,  lat2,  lng2) {
    var radioTierra = 6371;
    var dLat = toRadians(lat2 - lat1);
    var dLng = toRadians(lng2 - lng1);
    var sindLat = Math.sin(dLat / 2);
    var sindLng = Math.sin(dLng / 2);
    var va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2) * Math.cos(toRadians(lat1)) * Math.cos(toRadians(lat2));
    var va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));
    var meters = radioTierra * va2;
    return meters;
  }

  function geoCompare(a, b){    
    var metersA = metersTo(a.mlat, a.mlon, latitud, longitud)
    var metersB = metersTo(b.mlat, b.mlon, latitud, longitud)
    return metersA < metersB ? -1 : 1;
  }
  
  function obtener_localizacion() {
//    var searchInput = document.getElementById('search-geopos');
//    searchInput.innerHTML = "";
    navigator.geolocation.getCurrentPosition(coordenadas);
  }

  function coordenadas(position) {
    latitud = position.coords.latitude;
    longitud = position.coords.longitude;
    estaciones = estaciones.sort(geoCompare)
    window.location.href=estaciones[0].link;
  }

  getEstaciones('estaciones.json',function(err, data) {
    if (err !== null) {
    } else {
      estaciones = data;
      var searchInput = document.getElementById('search-geopos');
      searchInput.addEventListener("click",obtener_localizacion);
      searchInput.innerHTML='&#x1f4cc;'
    }
  });

})

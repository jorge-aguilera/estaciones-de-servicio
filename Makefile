generate:
	groovy dump.groovy

antora:
	docker-compose run antora

deploy:
	netlify deploy  --prod --dir=build

all: generate antora deploy

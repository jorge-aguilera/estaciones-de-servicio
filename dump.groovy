import javax.net.ssl.HttpsURLConnection
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.OutputStream; 

// Create a trust manager that does not validate certificate chains like the default 

TrustManager[] trustAllCerts = [
        new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers()
            {
                return null;
            }
            public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType)
            {
                //No need to implement.
            }
            public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)
            {
                //No need to implement.
            }
        }
]

// Install the all-trusting trust manager
try 
{
    SSLContext sc = SSLContext.getInstance("SSL");
    sc.init(null, trustAllCerts, new java.security.SecureRandom());
    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
} 
catch (Exception e) 
{
    System.out.println(e);
}


url = "https://sedeaplicaciones.minetur.gob.es/ServiciosRESTCarburantes/PreciosCarburantes/EstacionesTerrestres/"
String xml = new InputStreamReader(url.toURL().openStream(), 'UTF-8').text

map = [:]

new XmlParser().parseText(xml).ListaEESSPrecio.EESSPrecio.each { eess ->
    estacion = [:]
    estacion.with{
        rotulo = eess.Rótulo.text().trim()
	    direccion = eess.Dirección.text().replaceAll(',',' ')
	    horario = eess.Horario.text()
	    latitud = eess.Latitud.text().replaceAll(',','.')
	    longitud = eess.Longitud_x0020__x0028_WGS84_x0029_.text().replaceAll(',','.')
	    precios = [:]
	    precios.'95 E5' = eess.Precio_x0020_Gasolina_x0020_95_x0020_E5.text().replaceAll(',','.')
	    precios.'98 E5' = eess.Precio_x0020_Gasolina_x0020_98_x0020_E5.text().replaceAll(',','.')
	    precios.'Gasoleo A' = eess.Precio_x0020_Gasoleo_x0020_A.text().replaceAll(',','.')
	    precios.'Gasoleo B' = eess.Precio_x0020_Gasoleo_x0020_B.text().replaceAll(',','.')
	    precios.'Gasoleo Premium' = eess.Precio_x0020_Gasoleo_x0020_Premium.text().replaceAll(',','.')
	    precios.'BioDiesel' = eess.Precio_x0020_Biodiesel.text().replaceAll(',','.')
	    precios.'BioEtanol' = eess.Precio_x0020_Bioetanol.text().replaceAll(',','.')
	    precios.'GNC' = eess.Precio_x0020_Gas_x0020_Natural_x0020_Comprimido.text().replaceAll(',','.')
	    precios.'GNL' = eess.Precio_x0020_Gas_x0020_Natural_x0020_Licuado.text().replaceAll(',','.')
	    precios.'GLP' = eess.Precio_x0020_Gases_x0020_licuados_x0020_del_x0020_petróleo.text().replaceAll(',','.')
	    precios.'Hidrogeno' = eess.Precio_x0020_Hidrogeno.text().replaceAll(',','.')
    }

    provincia = map[eess.IDProvincia.text()] ?: [ name: eess.Provincia.text(), municipios:[:], estaciones:[]]
    municipio = provincia.municipios[eess.IDMunicipio.text()] ?: [name: eess.Municipio.text(), localidades:[:]]
    localidad = municipio.localidades[eess.Localidad.text()] ?: [ ]

    localidad.add estacion
    provincia.estaciones.add estacion

    municipio.localidades[eess.Localidad.text()] = localidad
    provincia.municipios[eess.IDMunicipio.text()] = municipio
    map[eess.IDProvincia.text()] = provincia
}

nav = new File('docs/modules/ROOT/nav.adoc')
nav.parentFile.mkdirs()

nav.text = ""
map.each{ kvp ->
    provincia = kvp.value
    nav << "* xref:${kvp.key}.adoc[$provincia.name]\n"
    provincia.municipios.each{ kvm ->
        municipio = kvm.value
        nav << "** xref:${kvp.key}.adoc#${municipio.name.toLowerCase().replaceAll(' ','_')}[$municipio.name]\n"
    }
}

geoposicion = new File('estaciones.json')
geoposicion.text = "[\n"

index = new File("docs/modules/ROOT/pages/index.adoc")
index.parentFile.mkdirs()
index.text = new File("index.adoc").text
index << "\n\nÚltima actualización ${new Date().format('yyyy-MM-dd HH:mm')}\n"

counter=0
map.each{ kvp ->
    provincia = kvp.value
    file = new File("docs/modules/ROOT/pages/${kvp.key}.adoc")
    file.parentFile.mkdirs()
    file.text ="= $provincia.name\n:tabs:\n\n"
    file << "[#baratas]\n"
    file << "== Estaciones más baratas por tipo\n"
    [ ['95 E5','98 E5'],
      ['Gasoleo A','Gasoleo B','Gasoleo Premium'],
      ['GNC','GNL','GLP','BioDiesel','BioEtanol',],
    ].each{ g->
        file << "[tabs]\n====\n"
        g.each{ c ->
            file << "$c::\n+\n--\n"
            top = provincia.estaciones.sort{ (it.precios[c] ?: Double.MAX_VALUE) as double }.take(4)
            top.findAll{ it.precios[c] }.each{ topestacion->
                file << "- <<$topestacion.direccion, ${topestacion.precios[c] ?: ''}€ ($topestacion.direccion)>>\n" 
            }
            file <<"--\n"
        }
        file << "\n====\n"
    }

    provincia.municipios.each{ kvm ->        
        municipio = kvm.value
        id = "${municipio.name.toLowerCase().replaceAll(' ','_')}"
        file << "[#$id]\n"
        file << "== $municipio.name \n\n"        
        municipio.localidades.each{ kvl ->            
            kvl.value.sort{it.direccion}.each{ estacion ->
                println "$estacion.direccion"
                id = "estacion_${counter++}"
                file << "[#$id]\n"
                file << "=== $estacion.direccion \n\n"
                file << "*$estacion.rotulo* $estacion.horario \n\n"
                file << "https://www.openstreetmap.org/?mlat=$estacion.latitud&mlon=$estacion.longitud#map=17/$estacion.latitud/$estacion.longitud[Ver en mapa,window=_blank]\n\n"                
                file << "[TIP]\n====\n"
                estacion.precios.findAll{ it.value }.each{
                    file << "* _${it.key}_ a *${it.value}* €\n"
                }
                file << "====\n\n"

                geoposicion << """{"link":"${kvp.key}.html#$id","mlat":$estacion.latitud,"mlon":$estacion.longitud},\n"""
            }
        }
    }
}
geoposicion << """{"link":"index.html","mlat":0,"mlon":0}]"""
